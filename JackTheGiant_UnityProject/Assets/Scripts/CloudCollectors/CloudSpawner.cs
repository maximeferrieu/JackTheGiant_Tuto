﻿using UnityEngine;
using System.Collections;

public class CloudSpawner : MonoBehaviour {

    [SerializeField] 
    private GameObject[] clouds;
    [SerializeField] 
    private float distanceBetweenClouds = 3f;
    private float minX, maxX;
    private float lastCloudPositionY;
    private float controlX;

    [SerializeField]
    private GameObject[] collectibles;
    [SerializeField] 
    private GameObject player;

    void Awake () 
    {
        controlX = 0f;
        SetMinAndMaxX();
        CreateClouds();
	}

    void Start()
    {
        PositionThePlayer();
    }

    //Calculation of the X limit we want our clouds to spawn 
    //on the LEFT and on the RIGHT side.
    void SetMinAndMaxX()
    {
        //Calculation of the RIGHT screen edge pixel to world unit
        //That's the RIGHT limit of the game in world units
        Vector3 bounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f));

        // Setting the RIGHT limit with a 0.5 WU offset to make sure the cloud sprite fits entirely in the game view
        maxX = bounds.x - 0.5f;

        // Setting the LEFT limit with the offset, taking the opposite of the RIGHT limit
        minX = -bounds.x + 0.5f;
    }

    // Randomizes the clouds positions in the array
    void Shuffle(GameObject[] arrayToShuffle)
    {
        for (int i = 0; i < arrayToShuffle.Length; i++)
        {
            GameObject temp = arrayToShuffle[i];
            int random = Random.Range(i, arrayToShuffle.Length);
            arrayToShuffle[i] = arrayToShuffle[random];
            arrayToShuffle[random] = temp;
        }
    }

    //Handles the disposition of the clouds
    void CreateClouds()
    {
        // Randomizes the clouds positions in the array
        Shuffle(clouds);

        float positionY = 0f;

        // Disposition of the cloud for constraining the disposition 
        // to a back a forth pattern spreading over 4 steps.
        for (int i = 0; i < clouds.Length; i++)
        {
            Vector3 temp = clouds[i].transform.position;
            temp.y = positionY;

            // spreading over 4 steps
            if (controlX == 0f)
            {
                temp.x = Random.Range(0.0f, maxX);
                controlX = 1f;
            }
            else if (controlX == 1f)
            {
                temp.x = Random.Range(0.0f, minX);
                controlX = 2f;
            }
            else if (controlX == 2f)
            {
                temp.x = Random.Range(1.0f, maxX);
                controlX = 3f;
            }
            else if (controlX == 3f)
            {
                temp.x = Random.Range(-1.0f, minX);
                controlX = 0f;
            }

            lastCloudPositionY = positionY;
            clouds[i].transform.position = temp;

            // Placing the clouds downwards decreasing the Y position
            // by a fixed amount set in distanceBetweenClouds.
            positionY -= distanceBetweenClouds;
        }
    }

    //Ensure that the first cloud is not a deadly one
    //and places the player above this cloud
    void PositionThePlayer()
    {
        GameObject[] darkClouds = GameObject.FindGameObjectsWithTag("Deadly");
        GameObject[] cloudsInGame = GameObject.FindGameObjectsWithTag("Cloud");

        //Ensure that the first cloud is not a deadly one
        for (int i = 0; i < darkClouds.Length; i++)
        {
            //We get that info checking if a Dark cloud is at the Y origin of clouds spawn point
            if (darkClouds[i].transform.position.y == 0f)
            {
                // If that's the case we swap the position with the first non lethal cloud.
                Vector3 t = darkClouds[i].transform.position;
                darkClouds[i].transform.position = new Vector3(cloudsInGame[0].transform.position.x,
                                                               cloudsInGame[0].transform.position.y,
                                                               cloudsInGame[0].transform.position.y);
                
                cloudsInGame[0].transform.position = t;
            }
        }

        // Placing the player above the first cloud
        Vector3 temp = cloudsInGame[0].transform.position;

        for (int i = 0; i < cloudsInGame.Length; i++)
        {
            if (temp.y < cloudsInGame[i].transform.position.y)
            {
                temp = cloudsInGame[i].transform.position;
            }
        }

        // Add an offset for the player to actualy sit above the first cloud.
        temp.y = 0.8f;
        player.transform.position = temp;
    }
}

