﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {


    public float speed = 8f;
    public float maxVelocity = 4f;

    private float initialXScale;
    private Rigidbody2D rb;
    private Animator anim;

    void Awake()
    {
        // Gets the X scale value on startup in order to 
        // reverse this value if the player goes LEFT
        initialXScale = transform.localScale.x;

        // Gets References to the corresponding components
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
	
	void Start () {
	
	}
	
	void FixedUpdate () 
    {
        PlayerMoveKeyboard();
	}

    // Handles the Player LEFT & RIGHT movements
    // Triggers the Walk an Idle animations
    void PlayerMoveKeyboard()
    {
        float forceX = 0f;

        // Returns an always positive value of the player's velocity
        float vel = Mathf.Abs(rb.velocity.x);
        float direction = Input.GetAxisRaw("Horizontal");

        // If the player press RIGHT
        if (direction > 0f)
        {
            // Calculate the force for the rigidbody
            if (vel < maxVelocity)
                forceX = speed;

            // Plays the Walk animation
            anim.SetBool("Walk", true);

            // Sets the sprites facing RIGHT
            Vector3 temp = transform.localScale;
            temp.x = initialXScale;
            transform.localScale = temp;
        }
        // If the player press LEFT
        else if (direction < 0f)
        {
            // Calculate the force for the rigidbody
            // The negative speed indicates we are going negative on the X axis (LEFT)
            if (vel < maxVelocity)
                forceX = -speed;
            
            // Plays the Walk animation
            anim.SetBool("Walk", true);

            // Sets the sprites facing LEFT
            Vector3 temp = transform.localScale;
            temp.x = -initialXScale;
            transform.localScale = temp;
        }
        // If the player does'nt go in any direction
        else
        {
            // Plays the Idle animation
            anim.SetBool("Walk", false);
        }

        // Add the calculated force to the player rigidbody
        rb.AddForce(new Vector2(forceX, 0f));
    }
}
