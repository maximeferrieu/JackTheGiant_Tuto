﻿//This is responsible for X scaling the background image 
//whatever the screen resolution is to make sure it
//feets the screen size everytime.

using UnityEngine;
using System.Collections;

public class BackgroundScaler : MonoBehaviour {

	void Start () 
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        Vector3 tempScale = transform.localScale;

        //Width of the sprite in world units.
        float width = sr.sprite.bounds.size.x;

        //World height in world units is orthographic size of the camera * 2 for some reasons
        float worldHeight = Camera.main.orthographicSize * 2.0f;

        //World width in world units. 
        //Division of the world height by the image  format ratio (height * width in pixels) 
        float worldWidth = worldHeight / Screen.height * Screen.width;

        // Scale factor get by deviding the world width by the sprite size (both in world units)
        tempScale.x = worldWidth / width;

        //Assign the scale factor to the gameObject's transform
        transform.localScale = tempScale;
	}
}
